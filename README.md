# VectorPathsDemo

## Description
The goal of this repo is simply to practice a collaborated list various demo examples of vector drawables, graphics and paths. I guess you could say it's my journey to mastering this side of Android. All of these examples are compiled lists of vector graphics that you may use yourself or read the code to make your own in one of your projects! Happy coding!

### Want to contribute?
Simply pull the code down and make a PR. There's a very high chance I will include it!

### Things to Know
* Min SDK version is 28
* Project is written in Kotlin and will likely stay 100% Kotlin unless there are unique circumstances

### Resources I've used
* https://medium.com/androiddevelopers/playing-with-paths-3fbc679a6f77
* http://www.curious-creature.com/2013/12/21/android-recipe-4-path-tracing/