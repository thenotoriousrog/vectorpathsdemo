package com.thenotoriousrog.vectorpathsdemo.customviews

import android.content.Context
import android.graphics.*
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.thenotoriousrog.vectorpathsdemo.R
import java.lang.Exception

/**
 * This class is a custom view that draws text along a path.
 */
class TextPathView : View {

    private val TAG = "TextPathView"
    private val DEFAULT_TEXT = "Hello!"
    private val DEFAULT_TEXT_COLOR = ContextCompat.getColor(context, R.color.black)
    private val DEFAULT_PATH_COLOR: Int = Color.LTGRAY

    private var text: String = DEFAULT_TEXT
    private var textColor: Int = DEFAULT_TEXT_COLOR
    private var pathColor: Int = DEFAULT_PATH_COLOR
    private val path = Path()
    private val pathPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG)

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {

        if(attrs != null) {
            pullAttributes(attrs)
        }

        pathPaint.style = Paint.Style.STROKE
        pathPaint.color = pathColor
        pathPaint.strokeWidth = 3.0f

        // TODO: determine if I need a background resource!!!

        textPaint.style = Paint.Style.FILL_AND_STROKE
        textPaint.color = textColor
        textPaint.textSize = 200f

        val pathMeasure = PathMeasure(path, false)
        val length = pathMeasure.length
        pathPaint.pathEffect = CornerPathEffect(length)
    }

    /**
     * Starts pulling information from XML for use in this view.
     */
    private fun pullAttributes(attrs: AttributeSet) {
        val typedArr = context.obtainStyledAttributes(attrs, R.styleable.TextPathView)
        try {
            this.text = typedArr.getString(R.styleable.TextPathView_text) ?: DEFAULT_TEXT
            this.textColor = typedArr.getColor(R.styleable.TextPathView_textColor, DEFAULT_TEXT_COLOR)
            this.pathColor = typedArr.getColor(R.styleable.TextPathView_pathColor, DEFAULT_PATH_COLOR)

        } catch (ex: Exception) {
            Log.e(TAG, "Error parsing styled attributes! Using default attributes!", ex)
            this.text = DEFAULT_TEXT
            this.textColor = DEFAULT_TEXT_COLOR
            this.pathColor = DEFAULT_PATH_COLOR
        }
        finally {
            typedArr.recycle()

        }
    }

    /**
     * Sets the text color of this text path
     * @param color - the color that we want to set in Int form i.e. R.color.awesome_color
     * **Note: Default color is black
     */
    fun setTextColor(color: Int) {
        this.textColor = ContextCompat.getColor(context, color)
        this.textPaint.color = textColor
    }

    /**
     * Sets the text to be drawn across a path.
     * @param text - the text to draw
     * **Note: Default text is "Hello!"
     */
    fun setText(text: String) {
        this.text = text
    }

    /**
     * Sets the color of the path that the text is drawn on/
     * @param color - the color to be changed to
     * **Note: the default color is Color.LTGRAY
     */
    fun setPathColor(color: Int) {
        this.pathColor = ContextCompat.getColor(context, color)
        this.pathPaint.color = pathColor
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.drawTextOnPath(text, path, 300.0f, 300.0f, textPaint)
    }

}