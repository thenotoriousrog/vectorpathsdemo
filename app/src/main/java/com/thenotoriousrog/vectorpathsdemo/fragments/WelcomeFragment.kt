package com.thenotoriousrog.vectorpathsdemo.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.thenotoriousrog.vectorpathsdemo.R
import com.thenotoriousrog.vectorpathsdemo.customviews.TextPathView

class WelcomeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val rootView = inflater.inflate(R.layout.welcome_fragment, container, false)
        val textPathView: TextPathView = rootView.findViewById(R.id.text_path_view) // not used at the moment.
        return rootView
    }


}