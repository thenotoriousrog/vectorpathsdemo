package com.thenotoriousrog.vectorpathsdemo.mainactivity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import com.astuetz.PagerSlidingTabStrip
import com.thenotoriousrog.vectorpathsdemo.R

class MainActivity : AppCompatActivity() {

    private lateinit var viewPager: ViewPager
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    private lateinit var slidingTabStrip: PagerSlidingTabStrip

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.viewPager = findViewById(R.id.view_pager)
        this.viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        this.slidingTabStrip = findViewById(R.id.pager_sliding_tab_strip)

        this.viewPager.adapter = this.viewPagerAdapter

        supportActionBar?.title = getString(R.string.app_name)
    }
}
