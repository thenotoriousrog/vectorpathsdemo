package com.thenotoriousrog.vectorpathsdemo.mainactivity

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.thenotoriousrog.vectorpathsdemo.backend.Constants
import com.thenotoriousrog.vectorpathsdemo.fragments.WelcomeFragment

/**
 * Used to help design the page items in the viewpager
 */
class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private val TOTAL_FRAGMENTS = 1 // hard coded as this is just a demo and nothing needs to be automated yet.

    override fun getItem(pos: Int): Fragment? {
        return when(pos) {
            0 -> WelcomeFragment()
            else -> null // don't display anything if this occurs.
        }
    }

    override fun getCount(): Int {
        return TOTAL_FRAGMENTS
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position) {
            0 -> return Constants.WELCOME_TAB_TITLE
            else -> null
        }
    }

}